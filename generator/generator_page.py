from faker import Faker
from random import randint
import re
import string

class DataGenerator:

    def __init__(self):
        self.faker = Faker()

    @staticmethod
    def generate_random_post_code():
        post_code = str(randint(0000000000, 9999999999))
        return post_code

    def generate_first_name(self):
        post_code = self.generate_random_post_code()
        digits = re.findall(r'\d{2}', post_code)
        first_name = ''
        for digit in digits:
            letter_index = int(digit) % 26
            first_name += string.ascii_lowercase[letter_index]
        return first_name

    def generate_random_last_name(self):
        return self.faker.last_name()
