import pytest
from pages.manager_page import ManagerPage
from pages.add_customers_page import AddCustomersPage
from pages.assertions.add_customers_assertions import AddCustomersAssertions
from pages.customers_list_page import CustomersPage
from pages.assertions.customers_list_assertions import CustomersAssertionsPage


class BaseTest:

    manager_page: ManagerPage
    add_customers_page: AddCustomersPage
    add_customers_assertions: AddCustomersAssertions
    customers_list_page: CustomersPage
    customers_list_assertions: CustomersAssertionsPage

    @pytest.fixture(autouse=True)
    def setup(self, request, driver):

        """this fixture provides multi-page access"""

        request.cls.driver = driver

        request.cls.manager_page = ManagerPage(driver)
        request.cls.add_customers_page = AddCustomersPage(driver)
        request.cls.customers_list_page = CustomersPage(driver)
        request.cls. add_customers_assertions = AddCustomersAssertions(driver)
        request.cls.customers_list_assertions = CustomersAssertionsPage(driver)
