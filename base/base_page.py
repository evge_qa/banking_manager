from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, timeout=10, poll_frequency=1)

    def open(self):
        self.driver.get(self.PAGE_URL)

    def check_page_is_opened(self):
        self.wait.until(EC.url_to_be(self.PAGE_URL))

    def element_is_visible(self, locator):
        return self.wait.until(EC.visibility_of_element_located(locator))

    def elements_are_visible(self, locator):
        return self.wait.until(EC.visibility_of_all_elements_located(locator))

    def is_clickable(self, locator):
        return self.wait.until(EC.element_to_be_clickable(locator))

    def click_btn(self, locator):
        return self.is_clickable(locator).click()

    def get_text(self, locator):
        return self.element_is_visible(locator).text

    def go_to_alert(self):
        return self.wait.until(EC.alert_is_present())
