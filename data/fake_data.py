from generator.generator_page import DataGenerator

class Data:

    generator = DataGenerator()

    post_code = generator.generate_random_post_code()
    first_name = generator.generate_first_name()
    last_name = generator.generate_random_last_name()
