from selenium.webdriver.common.by import By
import allure
from base.base_page import BasePage

class CustomersPage(BasePage):

    PAGE_URL = "https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/list"

    FIRST_NAME_FILTER_BTN = (By.XPATH, "//a[contains(text(), 'First Name')]")
    CUSTOMERS_FIRST_NAMES = (By.XPATH,
                             "//table[@class='table table-bordered table-striped']//td[@class='ng-binding'][1]")

    @allure.step("Click 'Submit' button")
    def click_first_name_filter(self):
        self.click_btn(self.FIRST_NAME_FILTER_BTN)

    @allure.step("Get customers elements list")
    def get_customers_name_elements_list(self):
        first_names_elements_list = self.elements_are_visible(self.CUSTOMERS_FIRST_NAMES)
        return first_names_elements_list

    @allure.step("Get customers names list")
    def get_customers_name_list(self):
        first_names_elements_list = self.get_customers_name_elements_list()
        customers_list_name = [name.text for name in first_names_elements_list]
        return customers_list_name

    @allure.step("Find the customer with average name length and delete it")
    def find_customer_with_avg_length_name_and_delete(self):
        first_names_elements_list = self.get_customers_name_elements_list()
        first_names_length = [len(name.text) for name in first_names_elements_list]
        avg_length = sum(first_names_length) / len(first_names_length)
        closest_name_index = min(range(len(first_names_length)),
                                 key=lambda i: abs(first_names_length[i] - avg_length))
        closest_name = first_names_elements_list[closest_name_index].text
        delete_customer_btn_locator = (By.XPATH,
                                       f"//td[text()='{closest_name}']/following-sibling::td/button[text()='Delete']")
        self.click_btn(delete_customer_btn_locator)

    @allure.step("Check the new customer has been added")
    def delete_customer_by_name(self, first_name):
        delete_customer_btn_locator = (By.XPATH,
                                       f"//td[text()='{first_name}']/following-sibling::td/button[text()='Delete']")
        self.click_btn(delete_customer_btn_locator)
