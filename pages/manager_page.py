from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import allure
from base.base_page import BasePage

class ManagerPage(BasePage):

    PAGE_URL = "https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager"

    ADD_CUSTOMER_BTN = (By.XPATH, "//button[contains(text(), 'Add Customer')]")
    SHOW_CUSTOMERS_BTN = (By.XPATH, "//button[@ng-click='showCust()']")

    @allure.step("Click 'Add Customer' button")
    def click_add_customer_btn(self):
        self.wait.until(EC.element_to_be_clickable(self.ADD_CUSTOMER_BTN)).click()

    @allure.step("Click 'Customers' button")
    def click_customers_list_btn(self):
        self.wait.until(EC.element_to_be_clickable(self.SHOW_CUSTOMERS_BTN)).click()


