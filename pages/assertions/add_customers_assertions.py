import allure
from pages.add_customers_page import AddCustomersPage

class AddCustomersAssertions(AddCustomersPage):

    @allure.step("Check alert window message")
    def check_alert_message(self, expected_message):
        alert_text = self.get_alert_text()
        assert alert_text == expected_message, f'Actual alert message "{alert_text}"' \
                                               f' does not match expected message "{expected_message}"'

    @allure.step("Check alert window success message")
    def check_alert_success_message(self):
        assert self.get_alert_text() == "Customer added successfully with customer id :6", \
            'Unexpected message. Check that customer should be added'

    @allure.step("Check alert window unsuccessful message")
    def check_alert_unsuccessful_message(self):
        assert self.get_alert_text() == "Please check the details. Customer may be duplicate.", \
            'Unexpected message. Check that customer should not be added'
