import allure
from pages.customers_list_page import CustomersPage


class CustomersAssertionsPage(CustomersPage):

    @allure.step("Check the First Name filter from A-Z")
    def check_first_name_filter_from_a_z(self):
        customers_list_name = self.get_customers_name_list()
        sorted_customers_list_name = sorted(customers_list_name)
        assert customers_list_name == sorted_customers_list_name,\
            "The First Name filter from A-Z does not work correctly"

    @allure.step("Check the First Name filter from Z-A")
    def check_first_name_filter_from_z_a(self):
        customers_list_name = self.get_customers_name_list()
        sorted_customers_list_name = sorted(customers_list_name)[::-1]
        assert customers_list_name == sorted_customers_list_name,\
            "The First Name filter from Z-A does not work correctly"

    @allure.step("Check that the customer with average first name has been deleted")
    def check_customer_with_avg_length_name_is_deleted(self):
        customers_list_name = self.get_customers_name_list()
        closest_name = self.find_customer_with_avg_length_name_and_delete()
        assert closest_name not in customers_list_name, f"The First name '{closest_name}'isn't deleted"

    @allure.step("Check the new customer has been added")
    def check_customer_is_added(self, first_name):
        customers_list_name = self.get_customers_name_list()
        assert first_name in customers_list_name, f"The customer - '{first_name}' isn`t added"

    @allure.step("Check if the new customer has been added to the list or not")
    def check_customer_is_added_or_not_added(self, first_name, valid_data=True):
        customers_list_name = self.get_customers_name_list()
        if valid_data:
            assert first_name in customers_list_name, f"The customer - '{first_name}' isn't added"
            self.delete_customer_by_name(first_name)
        else:
            assert first_name not in customers_list_name, f"The customer - '{first_name}' should not be added"