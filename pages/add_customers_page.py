from selenium.webdriver.common.by import By
import allure
from base.base_page import BasePage

class AddCustomersPage(BasePage):

    PAGE_URL = "https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/addCust"

    FIRST_NAME_FIELD = (By.XPATH, "//input[@ng-model='fName']")
    LAST_NAME_FIELD = (By.XPATH, "//input[@ng-model='lName']")
    POST_CODE_FIELD = (By.XPATH, "//input[@ng-model='postCd']")
    SUBMIT_ADD_CUSTOMER_BTN = (By.XPATH, "//button[@class='btn btn-default']")

    @allure.step("Input First Name")
    def input_first_name(self, first_name):
        self.is_clickable(self.FIRST_NAME_FIELD).send_keys(first_name)

    @allure.step("Input Last Name")
    def input_last_name(self, last_name):
        self.is_clickable(self.LAST_NAME_FIELD).send_keys(last_name)

    @allure.step("Input Post code")
    def input_post_code(self, post_code):
        self.is_clickable(self.POST_CODE_FIELD).send_keys(post_code)

    @allure.step("Click 'Submit' button")
    def click_submit_btn(self):
        self.click_btn(self.SUBMIT_ADD_CUSTOMER_BTN)

    @allure.step("Get alert window message text")
    def get_alert_text(self):
        alert_window = self.go_to_alert()
        return alert_window.text

    @allure.step("Click alert window 'ok' button")
    def click_alert_btn(self):
        click_ok = self.go_to_alert()
        return click_ok.accept()
