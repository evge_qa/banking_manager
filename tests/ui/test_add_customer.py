import allure
import pytest
from base.base_test import BaseTest
from data.fake_data import Data

@allure.feature("Add Customer feature")
class TestAddCustomerPage(BaseTest):

    @allure.story("Add a new customer with valid / invalid data")
    @allure.severity("Critical")
    @pytest.mark.UI
    @allure.testcase("TC_001.001.001,  TC_001.001.002")
    @pytest.mark.xdist_group(name="group1")
    @pytest.mark.parametrize('first_name, last_name, post_code, alert_message, valid_data', [
        (Data.first_name, Data.last_name, Data.post_code, "Customer added successfully with customer id :6", True),
        (' ', Data.last_name, Data.post_code, "Please check the details. Customer may be duplicate.", False)])
    def test_add_customer(self, first_name, last_name, post_code, alert_message, valid_data):
        self.manager_page.open()
        self.manager_page.click_add_customer_btn()
        self.add_customers_page.input_first_name(first_name)
        self.add_customers_page.input_last_name(last_name)
        self.add_customers_page.input_post_code(post_code)
        self.add_customers_page.click_submit_btn()
        self.add_customers_page.get_alert_text()
        self.add_customers_assertions.check_alert_message(alert_message)
        self.add_customers_page.click_alert_btn()
        self.manager_page.click_customers_list_btn()
        self.customers_list_assertions.check_customer_is_added_or_not_added(first_name, valid_data)
