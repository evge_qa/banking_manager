import allure
import pytest
from base.base_test import BaseTest


@allure.feature("Customers feature")
class TestCustomersPage(BaseTest):

    @pytest.mark.xfail
    @allure.story("Sorting customers by the First name from A to Z")
    @allure.severity("Minor")
    @pytest.mark.UI
    @allure.testcase("TC_001.002.001")
    @pytest.mark.xdist_group(name="group2")
    def test_sort_customers_by_first_name_a_z(self):
        self.manager_page.open()
        self.manager_page.check_page_is_opened()
        self.manager_page.click_customers_list_btn()
        self.customers_list_page.check_page_is_opened()
        self.customers_list_page.click_first_name_filter()
        self.customers_list_assertions.check_first_name_filter_from_a_z()

    @pytest.mark.xfail
    @allure.story("Sorting customers by the First name from Z to A")
    @allure.severity("Minor")
    @pytest.mark.UI
    @allure.testcase("TC_001.002.002")
    @pytest.mark.xdist_group(name="group2")
    def test_sort_customers_by_first_name_z_a(self):
        self.manager_page.open()
        self.manager_page.check_page_is_opened()
        self.manager_page.click_customers_list_btn()
        self.customers_list_page.check_page_is_opened()
        self.customers_list_page.click_first_name_filter()
        self.customers_list_page.click_first_name_filter()
        self.customers_list_assertions.check_first_name_filter_from_z_a()

    @allure.story("Deleting a client name from a table with average length")
    @allure.severity("Normal")
    @pytest.mark.UI
    @allure.testcase("TC_001.003.001")
    @pytest.mark.xdist_group(name="group3")
    def test_delete_first_name(self):
        self.manager_page.open()
        self.manager_page.check_page_is_opened()
        self.manager_page.click_customers_list_btn()
        self.customers_list_page.check_page_is_opened()
        self.customers_list_page.find_customer_with_avg_length_name_and_delete()
        self.customers_list_assertions.check_customer_with_avg_length_name_is_deleted()
