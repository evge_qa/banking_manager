
#### Test case TC_001.001.001.001
- Test Case Number: TC_001.001.001.001
- Test case name: Add Customer
- Description: Checks whether a new customer can be added
- Prerequisites: The user must have access to the (Add Customer) page for adding a new customer. 
When creating test data for the Post Code and First Name fields, you must:
1.1 generate a 10 digit number for the Post Code field.
1.2 for the First Name field generate a name based on the Post Code according to the following logic:
1) conditionally divide Post Code into two-digit digits (5 digits)
2) Convert each digit into a letter of the English alphabet in order from 0 to 25. 
If the digit is greater than 25, we start with 26 as 0. I.e. 0 is a, 26 is also a, 52 is also a, etc.
- Execution steps:
1. Go to the add new customer page.
2. Fill in the fields (First Name, Last Name, Post Code).
3. Click the "Add Customer" button.
4. Check that the new customer has appeared in the list of customers.
- Expected result: A message is displayed in the pop-up window "Customer added successfully with customer id :7".
The new customer has been successfully created and is displayed correctly in the customer list.
- Actual result: The message "Customer added successfully with customer id :7" is displayed in the pop-up window.
The new customer was successfully created and is displayed correctly in the customer list.
- Status: Passed

#### Test case TC_001.001.002
- Test case number: TC_001.001.002
- Test case name: Add Customer with invalid data
- Description: Test whether a new customer with invalid data can be added.
- Prerequisites: 
The user must have access to the (Add Customer) page of adding a new customer.
Test data: for First Name input field = ' ' (empty string), for Last Name input field = generate last name,
for Post Code input field = generate a 10 digit number.
- Execution steps:
1. Go to the add new customer page.
2. Fill in the fields (First Name, Last Name, Post Code).
3. Click the "Add Customer" or "Add" button.
4. Check that the new customer has appeared in the list of customers.
- Expected result: A pop-up window will display the message "Please check the details. Customer may be duplicate."
A new customer with invalid details is NOT displayed in the customer list.
- Actual result: A message is displayed in the pop-up window "Please check the details. Customer may be duplicate."
The new customer with invalid data is NOT displayed in the customer list.
- Status: Passed

#### Test case TC_001.002.001
- Test case number: TC_001.002.001
- Test case name: Sorting customers by First Name (First Name)
- Description: Checking whether the sorting of customers by name A-Z is correct.
- Prerequisites: Must have access to the page with the list of customers (Customers)
- Execution Steps:
1. Access the page with the list of customers.
2. Click on the sort button (First Name).
3. Check that the sorted list matches the expected order.
- Expected result: The client list is successfully sorted by name and matches the expected order - A-Z.
- Actual Result: The client list is NOT sorted by name and does not match the expected order - A-Z.
- Status: Unsuccessful

#### Test Case TC_001.002.002
- Test Case Number: TC_001.002.002.002
- Test case name: Sorting customers by First Name (First Name)
- Description: Checking whether the sorting of clients by name Z-A is correct.
- Prerequisites: There must be access to the page with the list of customers (Customers)
- Execution steps:
1. Access the page with the list of customers.
2. Click on the sort button (First Name).
3. Click the sort button (First Name).
4. Check that the sorted list is in the expected order.
- Expected result: The client list is successfully sorted by name and matches the expected order - Z-A.
- Actual result: The client list is NOT sorted by name and does not match the expected order - Z-A.
- Status: Unsuccessful


#### Test case TC_001.003.001
- Test Case Number: TC_001.003.001
- Test case name: Deleting a client
- Description: Test whether a customer can be deleted from the customer list
- Prerequisites: There must be access to the customer list page and a selected customer to delete.
When searching for a customer to delete, you must: Get a list of names from the Customers table. Find out the length of each name,
then find the arithmetic mean of the resulting lengths and delete the customer with the name whose length is closer to the arithmetic mean. 
to the arithmetic mean.
Example: list of names - Albus, Neville, Voldemort. The lengths of the names are 5, 7, 9 respectively. 
The arithmetic mean of the lengths is 7, delete the name Neville.
- Execution steps:
1. Go to the client list page.
2. Select the client to delete.
3. Press the "Delete" button.
4. Check that the data of the deleted client is no longer displayed in the client list.
- Expected result: The selected client is successfully deleted from the list and the data is no longer displayed.
- Actual result: The selected client is successfully deleted from the list and the data is no longer displayed.
- Status: Passed