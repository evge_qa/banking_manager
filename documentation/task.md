Task
1. Compose detailed test cases according to the checklist of 3 cases described below.
2. In the programming language Java (version 11 or 17) or Python (version 3.10) create a
UI-autotests project based on the test cases. Attach the cases to this project as well (in the format of a 
text file format using Markdown).
3. In the project use:
Selenium Webdriver (preferably using Chrome browser)
One of the test frameworks: Java - TestNG, JUnit 4/5, Python - pytest.
One of the builders (for Java): Maven, Gradle.
4. Form the results of the test as a Merge Request/Pull Request (!!!) of the branch in which 
you were developing in the main branch on Gitlab/GitHub.
5. Additional task #1: Implement Allure report generation.
6. Additional task #2: Implement parallel launch of tests.
7. Additional task #3: Realise the launch in CI/CD system.

Checklist 3

Test Object:

https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager

Checklist:
1. Create Customer (Add Customer).
When creating test data for the Post Code and First Name fields, you must:
1.1 generate a 10-digit number for the Post Code field
1.2 for the First Name field generate a name based on the Post Code according to the following logic:
1) Post Code is conditionally divided into two-digit digits (we will get 5 digits)
2) Convert each digit into a letter of the English alphabet in order from 0 to 25. 
If the digit is greater than 25, we start with 26 as 0. I.e. 0 is a, 26 is also a, 52 is also a, etc.
Example: 0001252667 = abzap

Checklist 4

Test Object:

https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager

Checklist:

2. Sorting customers by name (First Name).

3. Deleting a client.

When searching for a customer to delete, you must:

Get a list of names from the Customers table. 
To find out the length of each name, then to find the average 
arithmetic mean of the resulting lengths and delete the client with the name whose length will be closer to the arithmetic mean (for Java it is required to use Stream API). 
to the arithmetic mean (for Java it is required to use Stream API).

Example: list of names - Albus, Neville, Voldemort. The lengths of the names are 5, 7, 9 respectively. 
The arithmetic mean of the lengths is 7, delete the name Neville.
